﻿using NUnit.Framework;
using System;
using System.Diagnostics;

#if DEBUG

namespace LightWorkFlow.Tests
{
    [TestFixture]
    public class WorkflowProcessorTests
    {
        public enum CarResult
        {
            Unknown,
            Success,
            Failure,
            FinalSuccess,
            FinalFailure,
            RareEdgeCase
        }

        private class CarState
        {
            public Boolean IsGasTankFull { get; set; }
            public Boolean? IsCoolantFull { get; set; }
            public Boolean IsOilFull { get; set; }

            public override String ToString()
            {
                return String.Format("Gas: {0} Coolant: {1} Oil: {2}", IsGasTankFull, IsCoolantFull, IsOilFull);
            }
        }

        [Test]
        public void SimplestPossibleWorkingWorkflowTest()
        {
            var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );

            var node = workflow.NewIntermediateNode( "node", state => CarResult.Success );
            node.IsCompletedBy( CarResult.Success ).ResultingIn( CarResult.FinalSuccess );

            workflow.ProcessAll();

            Assert.AreEqual( CarResult.FinalSuccess, workflow.CurrentNode.Result );
        }

        [Test]
        public void SlightlyHarderWorkingWorkflowTest()
        {
            var state = new CarState { IsGasTankFull = true, IsOilFull = false };
            var workflow = new WorkflowProcessor<CarResult, CarState>( state );

            var gasNode = workflow.NewIntermediateNode( "Check Gas",
                                            s => s.IsGasTankFull ? CarResult.Success : CarResult.Failure );

            var oilNode = workflow.NewIntermediateNode( "Check Oil",
                                            s => s.IsOilFull ? CarResult.Success : CarResult.Failure );

            gasNode
                .When( CarResult.Success ).Then( oilNode )
                .IsCompletedBy( CarResult.Failure ).ResultingIn( CarResult.FinalFailure );

            oilNode
                .IsCompletedBy( CarResult.Success ).ResultingIn( CarResult.FinalSuccess )
                .IsCompletedBy( CarResult.Failure ).ResultingIn( CarResult.FinalFailure )
                .ForUndefined( CarResult.RareEdgeCase );

            //could also specify terminal or active node:
            //.ForUndefined( workflow.TerminalNode( "undefined", CarResult.RareEdgeCase ) );

            workflow.ProcessAll();

            Assert.AreEqual( CarResult.FinalFailure, workflow.CurrentNode.Result );
        }

        [Test]
        public void CarDiagnosticWorkflowTest()
        {
            WorkflowNode<CarResult, CarState> gasNode;
            var carState = new CarState { IsGasTankFull = true, IsCoolantFull = true, IsOilFull = true };
            var workflow = SetupCarDiagnosticWorkflow( carState, out gasNode );
            workflow.NodeProcessed += NodeProcessedHandler;

            //  all ok test
            workflow.ProcessAll();

            Debug.WriteLine( "Final node: {0}", workflow.CurrentNode );
            Debug.WriteLine( "Final state: {0}", workflow.State );

            Assert.AreEqual( true, workflow.CurrentNode.IsTerminalNode );
            Assert.AreEqual( CarResult.FinalSuccess, workflow.CurrentNode.Result );

            Debug.WriteLine( "" );
            Debug.WriteLine( "" );

            //  fail oil (last one )
            carState = new CarState { IsGasTankFull = true, IsCoolantFull = true, IsOilFull = false };
            workflow.Reset( carState );
            workflow.ProcessAll();

            Debug.WriteLine( "Final node: {0}", workflow.CurrentNode );
            Debug.WriteLine( "Final state: {0}", workflow.State );

            Assert.AreEqual( true, workflow.CurrentNode.IsTerminalNode );
            Assert.AreEqual( CarResult.FinalFailure, workflow.CurrentNode.Result );

            Debug.WriteLine( "" );
            Debug.WriteLine( "" );

            //  fail gas (first one ), goes through battery & ultimately succeeds
            carState = new CarState { IsGasTankFull = false, IsCoolantFull = true, IsOilFull = true };
            workflow.Reset( carState );
            workflow.ProcessAll();

            Debug.WriteLine( "Final node: {0}", workflow.CurrentNode );
            Debug.WriteLine( "Final state: {0}", workflow.State );

            Assert.AreEqual( true, workflow.CurrentNode.IsTerminalNode );
            Assert.AreEqual( CarResult.FinalSuccess, workflow.CurrentNode.Result );

            //  rare edge case, coolant is ambiguous and no outcome defined for RareEdgeCase return value; expect default behavior
            carState = new CarState { IsGasTankFull = false, IsCoolantFull = null, IsOilFull = true };
            workflow.Reset( carState );
            workflow.ProcessAll();

            Debug.WriteLine( "Final node: {0}", workflow.CurrentNode );
            Debug.WriteLine( "Final state: {0}", workflow.State );

            Assert.AreEqual( CarResult.RareEdgeCase, workflow.CurrentNode.Result );
        }

        [Test]
        public void CarDiagnosticStateMachineSteppingTest()
        {
            WorkflowNode<CarResult, CarState> gasNode;
            var carState = new CarState { IsGasTankFull = true, IsCoolantFull = true, IsOilFull = false };
            var workflow = SetupCarDiagnosticWorkflow( carState, out gasNode );
            workflow.NodeProcessed += NodeProcessedHandler;

            //  run fresh
            var counter = 0;
            while( workflow.ProcessNext() )
                counter++;

            Assert.AreEqual( 3, counter );
            Assert.AreEqual( true, workflow.CurrentNode.IsTerminalNode );
            Assert.AreEqual( CarResult.FinalFailure, workflow.CurrentNode.Result );

            //  run w/o reset, expect repeat of last node
            counter = 0;
            while( workflow.ProcessNext() )
                counter++;

            Assert.AreEqual( 0, counter );
            Assert.AreEqual( true, workflow.CurrentNode.IsTerminalNode );
            Assert.AreEqual( CarResult.FinalFailure, workflow.CurrentNode.Result );

            //reset, expect full run
            workflow.Reset( carState );
            counter = 0;
            while( workflow.ProcessNext() )
                counter++;

            Assert.AreEqual( 3, counter );
            Assert.AreEqual( true, workflow.CurrentNode.IsTerminalNode );
            Assert.AreEqual( CarResult.FinalFailure, workflow.CurrentNode.Result );
        }

        [Test]
        public void BareWorkflowNoNodesFailsTest()
        {
            Assert.Throws<Exception>(
                () =>
                {
                    var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );

                    //  workflow can't process w/o any nodes.
                    workflow.ProcessAll();
                },
                "Node is null, cannot process.." );
        }

        [Test]
        public void BaseDefaultOutcomeTest()
        {
            Assert.Throws<Exception>(
                () =>
                {
                    var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );

                    //  node has no outcome added for "Success", so falls through to exception-throwing base default outcome.
                    workflow.NewIntermediateNode( "node", state => CarResult.Success );
                    workflow.ProcessAll();
                },
                "Should not arrive here; expect exception from default node when workflow falls through." );
        }

        [Test]
        public void SubstitutedProcessorDefaultOutcomeTest()
        {
            var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );

            //  default outcome overriden; base default throws exception, this one just terminates with Failure
            workflow.ForUndefined( workflow.NewTerminalNode( "override default", CarResult.Failure ) );
            workflow.NewIntermediateNode( "node", state => CarResult.Success );
            workflow.ProcessAll();

            Assert.AreEqual( CarResult.Failure, workflow.CurrentNode.Result );
        }

        [Test]
        public void SubstitutedNodeDefaultOutcomeTest()
        {
            var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );

            //  node should fail as there is no outcome for Success; should go to base default node, which throws
            //  instead we override at node level with ForUndefined, set a terminal node there which becomes output
            var node = workflow.NewIntermediateNode( "node", state => CarResult.Success );
            var undefinedNode = workflow.NewTerminalNode( "override default", CarResult.Failure );
            node.ForUndefined( undefinedNode );
            workflow.ProcessAll();

            Assert.AreEqual( CarResult.Failure, workflow.CurrentNode.Result );
        }

        [Test]
        public void SubstitutedNodeDefaultOutcomeRepeatedTest()
        {
            var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );

            //  node should fail as there is no outcome for Success; should go to base default node, which throws
            //  instead we override at node level with ForUndefined, set a terminal node there which becomes output
            var node = workflow.NewIntermediateNode( "node", state => CarResult.Success );
            var undefinedNode = workflow.NewTerminalNode( "override default", CarResult.Failure );
            var undefinedNode2 = workflow.NewTerminalNode( "override default2", CarResult.Failure );
            node.ForUndefined( undefinedNode );

            //  accidentally add another ForUndefined; should just go to this one
            node.ForUndefined( undefinedNode2 );

            workflow.ProcessAll();

            Assert.AreEqual( undefinedNode2.Name, workflow.CurrentNode.Name );
            Assert.AreEqual( CarResult.Failure, workflow.CurrentNode.Result );
        }

        [Test]
        public void DanglingOutcomeTest()
        {
            Assert.Throws<Exception>(
                () =>
                {
                    var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );

                    //  the node "endNode", and the node that leads to it "node", are not terminal. 
                    //  execution goes from node, to endNode; endNode has no outcomes defined for "Success", so falls through to default node (error)
                    var node = workflow.NewIntermediateNode( "node", state => CarResult.RareEdgeCase );
                    var endNode = workflow.NewIntermediateNode( "terminal", state => CarResult.Success );
                    node.When( CarResult.RareEdgeCase ).Then( endNode );
                    workflow.ProcessAll();
                },
                "Workflow fell through, should have hit default node and thrown exception." );
        }

        [Test]
        public void UndefinedNodeMustNotBeNullTest()
        {
            Assert.Throws<Exception>(
                () =>
                {
                    var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );
                    var node = workflow.NewIntermediateNode( "node", state => CarResult.RareEdgeCase );

                    //  adding null as undefined should throw
                    node.ForUndefined( null );
                },
                "Should have thrown exception for null undefined node." );
        }

        [Test]
        public void UndefinedNodeMustBeTerminalTest()
        {
            Assert.Throws<Exception>(
                () =>
                {
                    var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );
                    var node = workflow.NewIntermediateNode( "node", state => CarResult.RareEdgeCase );
                    var undefinedNode = workflow.NewIntermediateNode( "undefined", state => CarResult.Unknown );
                    undefinedNode.IsTerminalNode = false;

                    //  adding non-terminal as undefined should throw
                    node.ForUndefined( undefinedNode );
                },
                "Should have thrown exception b/c undefined node is not terminal." );
        }

        [Test]
        public void IntermediateNodeFunctionCannotBeNullTest()
        {
            Assert.Throws<Exception>(
                () =>
                {
                    var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );
                    workflow.NewIntermediateNode( "node", state => CarResult.RareEdgeCase );

                    //  null function should throw
                    workflow.NewIntermediateNode( "node2", null );
                },
                "Should have thrown exception for null node function." );
        }

        [Test]
        public void IntermediateNodeCannotBeTerminalTest()
        {
            Assert.Throws<Exception>(
                () =>
                {
                    var workflow = new WorkflowProcessor<CarResult, CarState>( new CarState() );
                    var node = workflow.NewIntermediateNode( "node", state => CarResult.RareEdgeCase );
                    var node2 = workflow.NewIntermediateNode( "node2", state => CarResult.Unknown );
                    node2.IsTerminalNode = true;

                    //  should throw b/c intermediate node2 marked terminal
                    node.When( CarResult.RareEdgeCase ).Then( node2 );
                },
                "Should have thrown exception for terminal intermediate node." );
        }

        private void NodeProcessedHandler( WorkflowNodeProcessedEventArgs<CarResult, CarState> e )
        {
            Debug.WriteLine( "NodeProcessedEvent:      ['{0}'] = '{1}'", e.Node.Name, e.Node.Result );
        }

        private static WorkflowProcessor<CarResult, CarState> SetupCarDiagnosticWorkflow( CarState carState, out WorkflowNode<CarResult, CarState> gasNode )
        {
            var gasFullFunction = new Func<CarState, CarResult>(
                state =>
                {
                    if( state.IsGasTankFull )
                        return CarResult.Success;

                    return CarResult.Failure;
                } );

            var batteryFullFunction = new Func<CarState, CarResult>( state => { return CarResult.Success; } );

            var coolantFullFunction = new Func<CarState, CarResult>(
                state =>
                {
                    if( !state.IsCoolantFull.HasValue )
                        return CarResult.RareEdgeCase;

                    if( state.IsCoolantFull.Value )
                        return CarResult.Success;

                    return CarResult.Failure;
                } );

            var oilFullFunction = new Func<CarState, CarResult>(
                state =>
                {
                    if( state.IsOilFull )
                        return CarResult.Success;

                    return CarResult.Failure;
                } );

            var workflow = new WorkflowProcessor<CarResult, CarState>( carState );

            gasNode = workflow.NewIntermediateNode( "Gas Checker", gasFullFunction );
            var batteryNode = workflow.NewIntermediateNode( "Battery Checker", batteryFullFunction );
            var coolantNode = workflow.NewIntermediateNode( "Coolant Checker", coolantFullFunction );
            var oilNode = workflow.NewIntermediateNode( "Oil Checker", oilFullFunction );

            gasNode
                .When( CarResult.Success ).Then( coolantNode )
                .When( CarResult.Failure ).Then( batteryNode );

            batteryNode
                .When( CarResult.Success ).Then( coolantNode )
                .IsCompletedBy( CarResult.Failure ).ResultingIn( CarResult.FinalFailure );

            coolantNode
                .When( CarResult.Success ).Then( oilNode )
                .ForUndefined( workflow.NewTerminalNode( "Rare Coolant Edge Case", CarResult.RareEdgeCase ) )
                .IsCompletedBy( CarResult.Failure ).ResultingIn( CarResult.FinalFailure );

            oilNode
                .IsCompletedBy( CarResult.Success ).ResultingIn( CarResult.FinalSuccess )
                .IsCompletedBy( CarResult.Failure ).ResultingIn( CarResult.FinalFailure );

            return workflow;
        }
    }
}

#endif