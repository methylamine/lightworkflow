﻿using System;
using System.Collections.Generic;

namespace LightWorkFlow
{
    public class WorkflowNode<TResult, TState>
    {
        private WorkflowNode<TResult, TState> _defaultOutcomeNode;
        private readonly Func<TState, TResult> _function;
        private readonly IDictionary<TResult, WorkflowNode<TResult, TState>> _outcomes = new Dictionary<TResult, WorkflowNode<TResult, TState>>();

        internal WorkflowNode( String name, Func<TState, TResult> function, WorkflowNode<TResult, TState> defaultOutcomeNode )
            : this( name, function, defaultOutcomeNode, default( TResult ) ) { }

        internal WorkflowNode( String name, Func<TState, TResult> function, WorkflowNode<TResult, TState> defaultOutcomeNode, TResult defaultResult )
        {
            Name = name;
            _function = function;
            Result = defaultResult;
            _defaultOutcomeNode = defaultOutcomeNode;

            if( function == null )
                IsTerminalNode = true;
        }

        public String Name { get; }
        public TResult Result { get; private set; }
        internal Boolean IsTerminalNode { get; set; }

        public WorkflowNodeBuilder When( TResult result )
        {
            return new WorkflowNodeBuilder( result, this );
        }

        public WorkflowNodeCompleter IsCompletedBy( TResult result )
        {
            return new WorkflowNodeCompleter( result, this );
        }

        public WorkflowNode<TResult, TState> ForUndefined( TResult finalResult )
        {
            var terminalNode = new WorkflowNode<TResult, TState>( "undefined", null, null, finalResult );
            terminalNode.IsTerminalNode = true;
            return ForUndefined( terminalNode );
        }

        public WorkflowNode<TResult, TState> ForUndefined( WorkflowNode<TResult, TState> defaultOutcomeNode )
        {
            if( defaultOutcomeNode == null )
                throw new Exception( "Default outcome node cannot be null." );

            if( !defaultOutcomeNode.IsTerminalNode )
                throw new Exception( "Default outcome node must be marked as terminal." );

            _defaultOutcomeNode = defaultOutcomeNode;
            return this;
        }

        private void AddOutcome( TResult resultValue, WorkflowNode<TResult, TState> resultNode )
        {
            if( _outcomes.ContainsKey( resultValue ) )
                throw new Exception( $"Duplicate result key detected: '{resultValue}'. Cannot add duplicate keys." );

            _outcomes.Add( resultValue, resultNode );
        }

        public virtual WorkflowNode<TResult, TState> Process( TState state )
        {
            if( IsTerminalNode )
                return null;

            if( _function == null )
                throw new Exception( $"Node '{Name}' has null Processing function, but is not marked as terminal." );

            var result = _function( state );

            if( !_outcomes.ContainsKey( result ) )
                return _defaultOutcomeNode;

            Result = result;

            return _outcomes[ result ];
        }

        public override String ToString()
        {
            return $"[{Name}] result: {Result}";
        }

        public class WorkflowNodeBuilder
        {
            private readonly WorkflowNode<TResult, TState> _parentNode;
            private readonly TResult _result;
            private WorkflowNodeBuilder() { }

            internal WorkflowNodeBuilder( TResult result, WorkflowNode<TResult, TState> parentNode )
            {
                _result = result;
                _parentNode = parentNode;
            }

            public WorkflowNode<TResult, TState> Then( WorkflowNode<TResult, TState> resultNode )
            {
                if( resultNode.IsTerminalNode )
                    throw new Exception( "An intermediate node cannot be marked terminal; workflow must be able to continue after intermediate node." );

                _parentNode.AddOutcome( _result, resultNode );
                return _parentNode;
            }
        }

        public class WorkflowNodeCompleter
        {
            private readonly WorkflowNode<TResult, TState> _parentNode;
            private readonly TResult _result;
            private WorkflowNodeCompleter() { }

            internal WorkflowNodeCompleter( TResult result, WorkflowNode<TResult, TState> parentNode )
            {
                _result = result;
                _parentNode = parentNode;
            }

            public WorkflowNode<TResult, TState> ResultingIn( TResult result )
            {
                var terminalNode = new WorkflowNode<TResult, TState>( "exit", null, null, result );
                terminalNode.IsTerminalNode = true;

                _parentNode.AddOutcome( _result, terminalNode );
                return _parentNode;
            }
        }
    }
}