﻿using System;

namespace LightWorkFlow
{
    public class WorkflowNodeProcessedEventArgs<TResult, TState> : EventArgs
    {
        public WorkflowNodeProcessedEventArgs() {}

        public WorkflowNodeProcessedEventArgs( WorkflowNode<TResult, TState> node )
        {
            Node = node;
        }

        public WorkflowNode<TResult, TState> Node { get; set; }

        public override String ToString()
        {
            return String.Format( "[{0}] {1}", typeof( WorkflowNodeProcessedEventArgs<TResult, TState> ).Name, Node );
        }
    }
}