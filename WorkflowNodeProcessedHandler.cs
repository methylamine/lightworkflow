﻿namespace LightWorkFlow
{
    public delegate void WorkflowNodeProcessedHandler<TResult, TState>( WorkflowNodeProcessedEventArgs<TResult, TState> e );
}