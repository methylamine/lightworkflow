﻿using System;

namespace LightWorkFlow
{
    public class WorkflowProcessor<TResult, TState>
    {
        private WorkflowNode<TResult, TState> _defaultOutcomeNode;

        public WorkflowProcessor( TState state )
        {
            State = state;
            _defaultOutcomeNode = new WorkflowNode<TResult, TState>( "default", s => { throw new Exception( "No outcome defined for this result value." ); }, null );
        }

        public TState State { get; private set; }
        public WorkflowNode<TResult, TState> StartNode { get; private set; }
        public WorkflowNode<TResult, TState> CurrentNode { get; private set; }

        public void ForUndefined( WorkflowNode<TResult, TState> defaultOutcomeNode )
        {
            _defaultOutcomeNode = defaultOutcomeNode;
        }

        public WorkflowNode<TResult, TState> NewTerminalNode( String name, TResult result )
        {
            var node = new WorkflowNode<TResult, TState>( name, null, null, result );
            node.IsTerminalNode = true;
            return node;
        }

        public WorkflowNode<TResult, TState> NewIntermediateNode( String name, Func<TState, TResult> function )
        {
            if( function == null )
                throw new Exception( "Processing function cannot be null. To create a terminal node, use TerminalNode method." );

            var node = new WorkflowNode<TResult, TState>( name, function, _defaultOutcomeNode, default( TResult ) );
            if( StartNode == null )
            {
                StartNode = node;
                CurrentNode = node;
            }

            return node;
        }

        public void ProcessAll()
        {
            while( ProcessNext() ) ;
        }

        public Boolean ProcessNext()
        {
            if( CurrentNode == null )
                throw new Exception( "Node is null, cannot process." );

            if( State == null )
                throw new Exception( "State is null, cannot process." );

            var node = CurrentNode.Process( State );

            OnNodeProcessed( CurrentNode );

            CurrentNode = node ?? CurrentNode;

            return node != null;
        }

        public void Reset( TState state )
        {
            CurrentNode = StartNode;
            State = state;
        }

        public event WorkflowNodeProcessedHandler<TResult, TState> NodeProcessed;

        private void OnNodeProcessed( WorkflowNode<TResult, TState> node )
        {
            if( NodeProcessed != null )
                NodeProcessed( new WorkflowNodeProcessedEventArgs<TResult, TState>( node ) );
        }
    }
}